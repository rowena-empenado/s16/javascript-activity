let num = Number(prompt("Enter a number: "));
console.log("First attempt: " + num);

while (isNaN(num) || num == "") {
  alert("Enter a valid number!");
  num = Number(prompt("Enter a number: "));
}

console.log("The number you provided is: " + num);

for (let i = num; num >= 0; i--) {
	
	if(i <= 0) {break;}

	if(i == 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}

	if(i < 50) {
		break;
	}

	if(i % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number." )
		continue;
	}

	if(i % 5 == 0) {
		console.log(i);
	}
}


let myString = "supercalifragilisticexpialidocious";

let myConsonants = "";

console.log(myString);

for(let x = 0; x < myString.length; x++) {

	if(myString[x] === "a" || 
		myString[x] === "e" || 
		myString[x] === "i" || 
		myString[x] === "o" || 
		myString[x] === "u" 
	) {
		continue;
	} else {
		myConsonants += myString[x];
	}
}
console.log(myConsonants);